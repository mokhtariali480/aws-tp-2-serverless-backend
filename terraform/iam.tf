# Resource: aws_iam_role for Lambda execution
resource "aws_iam_role" "s3_to_sqs_lambda_role" {
  name = "s3_to_sqs_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
      },
    ]
  })
}

# Resource: aws_iam_role_policy to allow S3 access
resource "aws_iam_role_policy" "s3_access" {
  name   = "s3_access_policy"
  role   = aws_iam_role.s3_to_sqs_lambda_role.id
  
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "s3:*",
        Resource = "${aws_s3_bucket.s3_job_offer_bucket.arn}/*"
      },
    ]
  })
}

# Resource: aws_iam_role_policy to allow sending messages to SQS
resource "aws_iam_role_policy" "sqs_send_message" {
  name   = "sqs_send_message_policy"
  role   = aws_iam_role.s3_to_sqs_lambda_role.id
  
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "sqs:SendMessage",
        Resource = aws_sqs_queue.job_offers_queue.arn
      },
    ]
  })
}

# Resource: aws_iam_role pour la fonction Lambda
resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {
  name = var.sqs_to_dynamo_lambda_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
      },
    ],
  })
}

# Resource: aws_iam_role_policy pour les permissions SQS
resource "aws_iam_role_policy" "sqs_permissions" {
  name   = "sqs_permissions"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes",
        ],
        Resource = "arn:aws:sqs:${var.region}:${data.aws_caller_identity.current.account_id}:${var.sqs_job_queue_name}"
      },
    ],
  })
}

# Resource: aws_iam_role_policy pour les permissions DynamoDB
resource "aws_iam_role_policy" "dynamodb_permissions" {
  name   = "dynamodb_permissions"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "dynamodb:PutItem",
        Resource = "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.current.account_id}:table/${var.dynamo_job_table_name}"
      },
    ],
  })
}

data "aws_caller_identity" "current" {}



