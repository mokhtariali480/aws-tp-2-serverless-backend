data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

# Resource: aws_lambda_function
resource "aws_lambda_function" "s3_to_sqs_lambda" {
  function_name = "s3_to_sqs_lambda"
  handler       = "index.handler"
  memory_size   = 512
  timeout       = 900
  runtime       = "nodejs18.x"

  filename         = "/home/ali/Desktop/aws-tp-2-serverless-backend/src/s3_to_sqs_lambda/build/s3_to_sqs_lambda.zip"
  source_code_hash = filebase64sha256("/home/ali/Desktop/aws-tp-2-serverless-backend/terraform/empty_lambda_code.zip")

  role = aws_iam_role.s3_to_sqs_lambda_role.arn

  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }
}

# Resource: aws_lambda_permission pour permettre l'exécution à partir du seau S3
resource "aws_lambda_permission" "allow_execution_from_s3" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_job_offer_bucket.arn
}



# Resource: aws_lambda_function pour traiter les messages de la SQS queue
resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  function_name = var.sqs_to_dynamo_lambda_name
  handler       = "index.handler"
  memory_size   = 512
  timeout       = 30
  runtime       = "nodejs18.x"

  filename         = data.archive_file.empty_zip_code_lambda.output_path
  source_code_hash = filebase64sha256(data.archive_file.empty_zip_code_lambda.output_path)

  role = aws_iam_role.sqs_to_dynamo_lambda_role.arn

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job_table.name
    }
  }
}

# Resource: aws_lambda_permission pour autoriser la file SQS à invoquer la fonction Lambda
resource "aws_lambda_permission" "allow_execution_from_sqs" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.job_offers_queue.arn
}

# Data source: aws_sqs_queue pour obtenir l'ARN de la file d'attente SQS
data "aws_sqs_queue" "job_offers_queue" {
  name = var.sqs_job_queue_name
}

# Data source: aws_dynamodb_table pour obtenir le nom de la table DynamoDB
data "aws_dynamodb_table" "job_table" {
  name = var.dynamo_job_table_name
}


# resource aws_lambda_function job_api_lambda



# resource aws_lambda_permission allow_api_gw

