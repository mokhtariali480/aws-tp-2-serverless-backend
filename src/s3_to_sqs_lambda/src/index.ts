import { Handler, S3Event } from 'aws-lambda';
import { GetObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { SendMessageBatchCommand, SQSClient } from "@aws-sdk/client-sqs";
import { parse } from 'papaparse';
import { Readable } from 'stream';

const s3Client = new S3Client({});
const sqsClient = new SQSClient({ region: "eu-west-1" });

const queueUrl = process.env.QUEUE_URL || '';

export const handler: Handler = async (event: S3Event): Promise<void> => {
    console.log('EVENT: \n' + JSON.stringify(event, null, 2));
    for (const record of event.Records) {
        const getObjectCommand = new GetObjectCommand({
            Bucket: record.s3.bucket.name,
            Key: record.s3.object.key
        });

        const getObjectCommandOutput = await s3Client.send(getObjectCommand);
        const bodyContents = await streamToString(getObjectCommandOutput.Body as Readable);
        const jsonData = parse(bodyContents, { header: true });
        const chunkedJobs = spliceIntoChunks(jsonData.data, 10);

        for (const jobs of chunkedJobs) {
            await sendMessageToSQS(jobs);
        }
    }
};

const streamToString = (stream: Readable): Promise<string> => {
    const chunks: Buffer[] = [];
    return new Promise((resolve, reject) => {
        stream.on('data', (chunk: Buffer) => chunks.push(chunk));
        stream.on('error', reject);
        stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf-8')));
    });
};

const spliceIntoChunks = (arr: any[], size: number): any[][] => {
    return Array.from({ length: Math.ceil(arr.length / size) }, (_, i) => arr.slice(i * size, i * size + size));
};

const sendMessageToSQS = async (bodies: any[]): Promise<void> => {
    const batch = bodies.map((item, index) => {
        const id = item.id && /^[a-zA-Z0-9-_]{1,80}$/.test(item.id) ? item.id : `id_${index}_${Date.now()}`;
        return {
            Id: id,
            MessageAttributes: {},
            MessageBody: JSON.stringify(item),
        };
    }).filter(entry => entry.Id); // Filter out any entries that do not have an Id

    const command = new SendMessageBatchCommand({
        Entries: batch,
        QueueUrl: queueUrl
    });

    try {
        const response = await sqsClient.send(command);
        console.log('Batch sent successfully:', response);
    } catch (error) {
        console.error('Error sending message batch:', error);
        console.log('Batch with error:', batch);
    }
};

